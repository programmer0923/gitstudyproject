import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

    //0. 필드 선언
    private ServerSocket serverSocket;
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private Scanner sc = new Scanner(System.in);
    private StringBuffer sbIn;
    private StringBuffer sbOut;

    //1. 서버 바인딩 및 리슨
    public void setServer(){
        try {
            serverSocket = new ServerSocket(12345);
            System.out.println("생성된 서버소켓 정보 : " + serverSocket);
            socket = serverSocket.accept();
            System.out.println("연결된 소켓 정보 : " + socket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //2. 스트림 설정
    public void setStream(){
        try {
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //3. 수신
    public void receive(){
        System.out.println("수신 시작");
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        sbIn = new StringBuffer(in.readUTF());
                        if(sbIn.equals("exit")){
                            System.out.println("상대방 나감");
                            break;
                        }else{
                            System.out.println("상대방 : " + sbIn.toString());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                closeAll();
            }
        });
        thread.start();
    }

    //4. 송신
    public void send(){
        System.out.println("송신 시작");
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    sbOut = new StringBuffer(sc.nextLine());
                    if(sbOut.equals("exit")){
                        closeAll();
                    }else{
                        try {
                            out.writeUTF(sbOut.toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        thread.start();
    }

    //5. 자원반납
    public void closeAll(){
        try {
            serverSocket.close();
            socket.close();
            in.close();
            out.close();
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Server(){
        setServer();
        setStream();
        receive();
        send();
    }

    public static void main(String[] args) {
        new Server();
    }



}
