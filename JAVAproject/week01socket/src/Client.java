import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    //0. 필드 선언
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private Scanner sc = new Scanner(System.in);
    private StringBuffer sbIn;
    private StringBuffer sbOut;

    //1. 소켓연결
    public void setSocket(){
        try {
            socket = new Socket("192.168.123.105",12345);
            System.out.println("나의 소켓(클라이언트) 정보 : " + socket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //9. 스트림 설정
    public void setStream(){
        try {
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //2. 송신
    public void send(){
        System.out.println("송신 시작");
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    sbOut = new StringBuffer(sc.nextLine());
                    if(sbOut.equals("exit")){
                        closeAll();
                    }else{
                        try {
                            out.writeUTF(sbOut.toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        thread.start();
    }

    //3. 수신
    public void receive(){
        System.out.println("수신 시작");
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        sbIn = new StringBuffer(in.readUTF());
                        if(sbIn.equals("exit")){
                            System.out.println("상대방 나감");
                            break;
                        }else{
                            System.out.println("상대방 : " + sbIn.toString());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                closeAll();
            }
        });
        thread.start();
    }

    //4. 자원반납
    public void closeAll(){
        try {
            socket.close();
            in.close();
            out.close();
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Client(){
        setSocket();
        setStream();
        send();
        receive();
    }

    public static void main(String[] args) {
        new Client();
    }

}
